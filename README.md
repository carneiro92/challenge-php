# challenge-php

* Durée : 2 après midi
* En solo (mais vous pouvez bien sûr vous entraider et bosser à plusieurs si vous préférez)
* Techno à utiliser : PHP From Scratch, Composer (pour l'autoloading), Bootstrap

Sujet : Faire une petite interface de gestion de ses objets connectés

En vous basant sur les diagrammes UML, créer les classes en PHP. L'idée est qu'on ait un
objet IoT qui contiendra tous nos devices qu'on pourra afficher avec la méthode renderDevices()

Chaque device a une fonction renderHTML() qui générera le HTML de l'appareil.

Faire ensuite un formulaire en bootstrap qui permettra de créer une instance de Device et
l'ajoutera via la méthode addDevice de la classe IoT.

La classe Smartphone est un type de Device avec des informations supplémentaires. 
Faire un affichage particulier pour cette classe avec la méthode renderHTML().

Modifier le formulaire pour qu'on puisse créer soit un Device classique soit un Smartphone
(ce qui rajoutera des champs supplémentaires)

![application class diagram](challenge-php.jpg)



## Aller plus loin
* Utiliser la session pour stocker l'état du tableau entre plusieurs requêtes
* Faire en sorte de pouvoir supprimer une instance existante
* Faire en sorte de pouvoir modifier une instance existante
* Créer un nouveau type de device
